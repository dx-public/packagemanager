#!/bin/bash

pmversion='0.5'
installdir='/etc/packagemanager'

if [ -z "$1" ]; then
echo "Please pass an option:"
echo "install - update/install script"
echo "self-update - update script"
echo "list - list available packages"
echo "update <pkg> - update/install package"
echo "upgrade <pkg> - upgrade/install package"
echo "remove - remove package manager"
echo "uninstall - remove package manager"
exit
fi

#echo "The arguments passed in are : $@"
#echo "The number of arguments passed in are : $#"

i=$(($#-1))
while [ $i -ge 0 ];
do
    if echo $1 | grep -q "^-"; then
     if [ "$1" = "-s"               ]; then shift; fi
     if [ "$1" = "-v"               ]; then echo "Version $pmversion"; exit; fi
     if [ "$1" = "--version"        ]; then echo "Version $pmversion"; exit; fi
     if [ "$1" = "-u"               ]; then curlurl=$2; shift; fi
     if [ "$1" = "--url"            ]; then curlurl=$2; shift; fi
    else
     if [ "$1" = 'list'             ]; then listpackages='yes'; fi
     if [ "$1" = 'install'          ]; then shouldinstall='yes'; fi
     if [ "$1" = 'self-update'      ]; then shouldinstall='yes'; fi
     if [ "$1" = 'remove'           ]; then shouldremove='yes'; fi
     if [ "$1" = 'uninstall'        ]; then shouldremove='yes'; fi
     if [ "$1" = 'update'           ]; then packageid=$2; if [ ! -z $3 ]; then shift; fi; fi
     if [ "$1" = 'upgrade'          ]; then packageid=$2; if [ ! -z $3 ]; then shift; fi; fi
     if [ "$1" = 'version'          ]; then echo "Version $pmversion"; exit; fi
    fi
    i=$((i-1))
    if [ ! -z $2 ]; then shift; fi
done

#------------------------------------------Start of Functions------------------------------------------

version() {
  echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'
}

checkpathwriteable() {
  for dr in $(printf "%b\n" "${PATH}" | tr ':' '\n' | grep -v "${installdir}")
  do
  mkdir -p ${dr}/testoperation 2>/dev/null
  if [ "$?" -eq 0 ]; then rmdir ${dr}/testoperation 2>/dev/null; echo ${dr}; return 0; fi
  rmdir ${dr}/testoperation 2>/dev/null
  #find $dr -maxdepth 0 ! \( -perm -u=w -o -perm -g=w \) && echo "$dr"
  done
  return 1;
}

link_binary() {
  origbin=$1
  newbin=$2
  #if [ -z "${pathbinw}" ]; then pathbinw=$(checkpathwriteable); fi
  if [ -z "${newbin}" ]; then newbin=${packageid}; fi

  #if [ ! -z "${pathbinw}" ]; then

   #if [ -f "${pathbinw}/${newbin}" ]; then mv ${pathbinw}/${newbin} ${pathbinw}/${newbin}.bak; fi
   #rm ${pathbinw}/${newbin} 2>/dev/null
   #ln -s ${origbin} ${pathbinw}/${newbin}
   if [ -f "$newbin" ]; then rm ${newbin} 2>/dev/null; fi

   #ln -s existing new
   if [ ! -e "$newbin" ]; then ln -s ${origbin} ${newbin}; fi

  #fi
}

check_installed() {
bindirs=''; bindir=''; dr=''; bdir=''; idir=''; mbindirs=''; otherpaths='';
binfile=$1
onlypm=$2
if [ -z "$binfile" ]; then return 1; fi
if [ -z "$onlypm" ]; then otherpaths=":${PATH}:/etc/${binfile}:/root:/home"; fi
for dr in $(printf "%b\n" "${installdir}${otherpaths}" | tr ':' '\n')
do
if [ ! -d "$dr" ]; then if [ "$dr" != "/root" ]; then mkdir -p $dr 2>/dev/null; fi; fi
if [ -d "$dr"  ]; then
  # bindir=`find $dr -type f -name "${binfile}" 2>/dev/null | while read file; do printf "$file:"; head -c 4 "$file"; done | grep 'ELF\|??' | cut -d ':' -f1`
  bindir=`find $dr -type f -name "${binfile}" 2>/dev/null | while read file; do printf "$file:"; grep -m 1 '' "$file" 2>/dev/null | grep -i "binary\|matches" 2>/dev/null; done | cut -d ':' -f1`
  if [ ! -z "$bindir" ]; then
   bindirs="$(printf "%b\n" "${bindirs}\n" "${bindir}\n" | sort -u | sed '/^$/d')"
  fi
fi
done
#if binary not found then search script(s) second
if [ -z "$bindirs" ]; then
 for dr in $(printf "%b\n" "${PATH}:${installdir}:/etc/${binfile}:/root:/home" | tr ':' '\n')
 do
 if [ -d "$dr"  ]; then
  bindir=`find $dr -type f -name "${binfile}" 2>/dev/null | while read file; do printf "$file:"; head -c 4 "$file"; done | grep '#!' | cut -d ':' -f1`
  if [ ! -z "$bindir" ]; then
   bindirs="$(printf "%b\n" "${bindirs}\n" "${bindir}\n" | sort -u | sed '/^$/d')"
  fi
 fi
 done
fi

 if [ $(printf "%b\n" "$bindirs" | wc -w) -gt 1 ]; then
  if printf "%b\n" "$bindirs" | grep -q "${installdir}/packages/${binfile}"; then idir=$(printf "%b\n" "$bindirs" | sed '/^$/d' | grep "${installdir}/packages/${binfile}" | sort -u | head -n 1); fi
  for bdir in $(printf "%b\n" "$bindirs" | sed '/^$/d' | sort -u | grep -v "${installdir}/packages/${binfile}")
  do
   if [ ! -z "$idir" ]; then
    mv $bdir $bdir.bak
    #ln -s $idir $bdir
    link_binary $idir $bdir
    #link_binary $idir ${binfile}
    mbindirs=$idir;
   fi
   if [ -z "$idir" ]; then
     if [ "$bdir" != $(printf "%b\n" "$bindirs" | head -n 1) ]; then
      mv $bdir $bdir.bak
      #ln -s $(printf "%b\n" "$bindirs" | head -n 1) $bdir
      link_binary $(printf "%b\n" "$bindirs" | head -n 1) $bdir
      #link_binary "$(printf "%b\n" "$bindirs" | head -n 1)" "${binfile}"
      mbindirs=$(printf "%b\n" "$bindirs" | head -n 1);
     fi
   fi
  done
  if [ -z "$(printf "%b\n" "$bindirs" | grep -v "${installdir}/packages/${binfile}")" ]; then mbindirs=$bindirs; fi

  binloc=$(printf "%b\n" "$mbindirs" | sort -u | sed '/^$/d' | head -n 1 ); 
  #check file is empty or not
  if grep -q . "${binloc}"; then
   printf "%b\n" "${binloc}"
  else
   return 1
  fi

  return 0;
 fi #end gt 1

 if [ $(printf "%b\n" "$bindirs" | wc -w) -eq 1 ]; then

  binloc=$(printf "%b\n" "$bindirs" | head -n 1 | tr -d '\n')
  #check file is empty or not
  if grep -q . "${binloc}"; then
   printf "%b\n" ${binloc}
  else
   return 1
  fi

  return 0
 fi # end eq 1

 #if [ ! -z "$bindirs" ]; then echo $(printf "%b\n" "$bindirs" | head -n 1 | tr -d '\n'); return 0; fi
 #else no pkg found
 return 1
}

retrydownload() {
#retrydownload curl https://example.com /dev/null
prog="$1"
url="$2"
file="$3"
alturl="$url"
if [ "$(echo "$url" | awk '/\/ipfs\/|\/ipns\/|.ipfs.|.ipns./ {print $0}')" = "$url" ]; then
 echo "ipfs/ipns url found"
 cidurl="$(echo "$url" | awk -F'.' '{ sub(/http:\/\/|https:\/\//, ""); print $1}')"
 filename="$(echo "$url" | awk -F / '{print $NF}')"  # or "$(echo ${url##*/})" 
 proto="$(echo $url | awk -F'/' '{print $1"//"}')"
 alturl="$(echo ${proto}${cidurl}.ipfs.4everland.io/${filename})"
fi

cmdprog=""
binpath=""
if [ "$prog" = "curl" ]; then
for DIR in $(echo "$PATH" | awk '{gsub(/:/," "); print}'); do if [ -f $DIR/curl ]; then binpath="$DIR/curl"; break; fi; done
#binpath=$(for DIR in $(echo "$PATH" | awk '{gsub(/:/," "); print}'); do pathexec=$(ls -l $DIR/* 2>/dev/null | awk '/curl/ {print $NF;exit}'); done; echo $pathexec)
#echo "cbinpath: $binpath pathexec:$pathexec path:$PATH dir:$DIR"
cmdprog="$binpath -L --doh-url https://1.1.1.1/dns-query -s -o $file"; 
fi

if [ "$prog" = "wget" ]; then
for DIR in $(echo "$PATH" | awk '{gsub(/:/," "); print}'); do if [ -f $DIR/wget ]; then binpath="$DIR/wget"; break; fi; done
#echo "wbinpath: $binpath pathexec:$pathexec path:$PATH dir:$DIR"
#echo "$(for DIR in $(echo "$PATH" | awk '{gsub(/:/," "); print}'); do echo "$DIR"; done)"
cmdprog="$binpath -q -O $file"
fi

if [ "$prog" = "gocurl" ]; then binpath="/etc/gocurl/gocurl"; cmdprog="/etc/packagemanager/packages/bin/gocurl"; fi

#echo "DEBUG***$cmdprog file:$file prog:$prog binpath:$binpath***"
if [ -z "$binpath" ]; then echo "No curl/wget present. Exiting"; exit; fi

i=0
while [ $i -lt 20 ]
do
    i=$((i + 1))
    if [ $i -ge 10 ]; then url="$alturl"; fi
    echo "downloading from $url using $prog and saving to $file"
    $($cmdprog $url > $file)
    result=$?
    if [ "$result" -eq 0 ] && [ -s "$file" ] && [ "$(awk '{if(NR==1) {print substr($1,1,1)}}' $file)" != "<" ]; then
        break
    fi
    echo "Error: Retry Download Again... $i time(s)"
    if [ $i -eq 20 ]; then echo "Error: please download file again"; break; fi
    sleep 3
done
#if [ $i -lt 20 ]; then echo "Successful after $i trie(s)"; fi
}

cache_file()
{
  if [ "${arg5}" = "debug" ]; then echo "cache_file function was called as : $@"; fi
  #default settings
  if [ ! -z "$1" ]; then arg1="$1"; else arg1='example.invalid:443'; fi #default to null url
  if [ ! -z "$2" ]; then arg2="$2"; else arg2='/dev/null'; fi #default to null file
  if [ ! -z "$3" ]; then arg3="$3"; else arg3='4'; fi #default use ipv4
  if [ ! -z "$4" ]; then arg4="$4"; else arg4='43200'; fi #default 1day in secs
  if [ ! -z "$5" ]; then arg5="$5"; else arg5='variable'; fi #default variable (console, debug, variable, file)

  if [ "${arg5}" = "debug" ]; then echo "url: $arg1"; fi
  if [ "${arg5}" = "debug" ]; then echo "file: $arg2"; fi
  if [ "${arg5}" = "debug" ]; then echo "proto: $arg3"; fi
  if [ "${arg5}" = "debug" ]; then echo "oldsecs: $arg4"; fi
  if [ "${arg5}" = "debug" ]; then echo "output: $arg5"; fi

  if ! echo $arg1 | grep -i -q '^http'; then echo ''; return; fi #not a valid http(s) url

  emptyfile=''
  directoryarg2=$(echo $arg2 | awk -F'/[^/]*$' '{print $1}')

  if [ -f "$arg2" ] && [ -s "$arg2" ]; then
    emptyfile=no
    if [ "${arg5}" = "debug" ]; then echo "file exists and has data"; fi
    if [ "$(( $(date +"%s") - $(date -r ${arg2} +%s) ))" -gt "$arg4" ]; then
     if [ ${arg5} = "debug" ] || [ ${arg5} = "file" ]; then echo "${arg2} file is older then ${arg4} seconds.. redownloading via ipv${arg3} from ${arg1}"; fi
      rm ${arg2}
      if [ ! -z "$directoryarg2" ] && [ $(echo "$directoryarg2" | grep "/packages/") ]; then rm -rf "$directoryarg2/**"; mkdir -p "$directoryarg2"; fi
      #curlcmd=`curl -${arg3}Ls "${arg1}" -o ${arg2}`
      curlcmd=`${installdir}/packages/bin/gocurl "${arg1}" > ${arg2}`
    fi
  fi

  if [ ! -f "${arg2}" ] || [ -z ${emptyfile} ]; then
   if [ ${arg5} = "debug" ] || [ ${arg5} = "file" ]; then echo "${arg2} file does not exist or empty... downloading via ipv${arg3} from ${arg1}"; fi
   #curlcmd=`curl -${arg3}Ls "${arg1}" -o ${arg2}`
   if [ ! -z "$directoryarg2" ] && [ $(echo "$directoryarg2" | grep "/packages/") ]; then rm -rf "$directoryarg2/**"; mkdir -p "$directoryarg2"; fi
   curlcmd=`${installdir}/packages/bin/gocurl "${arg1}" > ${arg2}`
  fi

  if grep -i -q -m 1 '^@' "${arg2}" 2>/dev/null; then arg5='file'; else arg5='variable'; fi
  #if ! grep -i -q -m 1 'binary\|matches' "$arg2" 2>/dev/null; then arg5='variable'; else arg5='file'; fi
  #if file -i ${arg2} | grep -q "text\|json"; then arg5='variable'; else arg5='file'; fi

  if [ "${arg5}" = "variable" ]; then cat ${arg2}; fi
  if [ "${arg5}" = "file"     ]; then echo "Binary saved to ${arg2}"; fi
  return
}

installpackage() {

  filename=$1
  packageid=$2

  #echo "arguments passed 1:$1 2:$2"

  if [ -z "$1" ] || [ -z "$2" ]; then echo "Please pass filename and packageid"; exit; fi
  if [ ! -d ${installdir}/packages/bin ]; then mkdir -p ${installdir}/packages/bin; fi

 filebse=${filename%.*}
 fileext=${filename##*.}
 if echo $filename | grep -q -i '\.gz\|\.bz2\|\.lzma'; then
  fileext=$(echo $filename | rev | cut -d '.' -f1,2 | rev);
  filebse=$(echo $filename | rev | cut -d '.' -f3- | rev);
 fi

 if ! (echo $fileext | grep -q "tar\|txt\|zip\|tgz\|gz\|bz2\|lzma\|tar.gz\|go\|sh\|dns\|deb"); then fileext=""; filebse="${filename}"; fi #binary (dot with version)
 if [ "$filebse" = "$fileext" ]; then fileext=""; filebse="${filename}"; fi #binary (no dot)

 echo "Filename: ${filename} base: ${filebse} ext: ${fileext}"

  #binary
  if [ "$fileext" = '' ]; then
   echo "Installing binary file..."
   if [ "${filename}" != "${packageid}" ]; then mv ${installdir}/packages/${packageid}/${filename} ${installdir}/packages/${packageid}/${packageid}; filename=${packageid}; fi
   chmod +x ${installdir}/packages/${packageid}/${filename}
   if [ ! -z "${packageid}" ]; then
    if [ -L ${installdir}/packages/bin/${packageid} ] || [ -e ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
    #ln -s ${installdir}/packages/${packageid}/${filename} ${installdir}/packages/bin/${packageid}
    #link_binary "${installdir}/packages/${packageid}/${filename}" "${packageid}"
    #link_binary "${installdir}/packages/${packageid}/${filename}" "${installdir}/packages/bin/${packageid}"
   fi
   binpath="${installdir}/packages/${packageid}/${filename}"
  fi

  if [ "$fileext" = 'zip' ]; then
   echo "Unzipping zip file..."
   unzip -qq -o ${installdir}/packages/${packageid}/${filename} -d ${installdir}/packages/${packageid}
   binaryloc=$(check_installed "${packageid}")
   if [ -z "$binaryloc" ]; then
      echo "Binary not found please choose a different package in json or manually compile it"
      echo ""
      exit
    else
      echo "Binary found: ${binaryloc}"
      chmod +x ${binaryloc}
      #if [ -L ${installdir}/packages/bin/${packageid} ] || [ -e ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
      #ln -s ${binaryloc} ${installdir}/packages/bin/${packageid}
      #link_binary "${binaryloc}" "${packageid}"
      #link_binary "${binaryloc}" "${installdir}/packages/bin/${packageid}"
    fi
    binpath="${binaryloc}"
  fi

  if [ "$fileext" = 'tgz' ]; then
   echo "Unpacking tgz file..."
   tar -xf ${installdir}/packages/${packageid}/${filename} -C ${installdir}/packages/${packageid}
   binaryloc=$(check_installed "${packageid}")
     if [ -z "$binaryloc" ]; then
      echo "Binary not found please choose a different package in json or manually compile it"
      echo ""
      exit
     else
      echo "Binary found: ${binaryloc}"
      chmod +x ${binaryloc}
      #if [ -L ${installdir}/packages/bin/${packageid} ] || [ -e ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
      #ln -s ${binaryloc} ${installdir}/packages/bin/${packageid}
      #link_binary "${binaryloc}" "${installdir}/packages/bin/${packageid}"
      if [ ! -z $pbin ]; then
       mv ${pbin} ${pbin}.bak;
         if [ "$binaryloc" = "$pbin" ]; then
         echo "tgz matches"
          #ln -s ${binaryloc} ${installdir}/packages/bin/${packageid}
          #link_binary "${binaryloc}" "${packageid}"
          #link_binary "${binaryloc}" "${installdir}/packages/bin/${packageid}"
         else
         echo "tgz dont match"
          #ln -s ${binaryloc} ${pbin}
          #link_binary "${binaryloc}" "${packageid}"
          #link_binary "${binaryloc}" "${pbin}"
         fi
      fi
    fi
    binpath="${binaryloc}"
  fi

  if [ "$fileext" = 'tar.gz' ]; then
   echo "Unpacking tar.gz file..."
   tar -xf ${installdir}/packages/${packageid}/${filename} -C ${installdir}/packages/${packageid}
   binaryloc=$(check_installed "${packageid}" "onlypmdir")

     if [ -z "$binaryloc" ]; then
      echo "Binary not found please choose a different package in json or manually compile it"
      echo ""
      exit
     else
      echo "Binary found: ${binaryloc}"
      #echo "PBin: ${pbin}"
      chmod +x "${binaryloc}"
      binpath="${binaryloc}"
      #if [ -L "${installdir}/packages/bin/${packageid}" ] || [ -e "${installdir}/packages/bin/${packageid}" ]; then rm ${installdir}/packages/bin/${packageid}; fi
      #ln -s ${binaryloc} ${installdir}/packages/bin/${packageid}
      #link_binary "${binaryloc}" "${packageid}"
      #link_binary "${binaryloc}" "${installdir}/packages/bin/${packageid}"
      if [ "${pbin}" != "${binaryloc}" ]; then installed=$(check_installed "${packageid}"); echo "updating other links"; fi
    fi
  fi


  if [ "$fileext" = 'go' ]; then
   echo "Installing go program"
   if [ ! `check_installed go` ]; then echo "Please install go via pm.sh or manually"; exit; fi
   curdir=$PWD
   cd ${installdir}/packages/${packageid}/
   echo "building deps..."
   if [ -f ${installdir}/packages/${packageid}/go.mod ]; then rm ${installdir}/packages/${packageid}/go.mod; fi
   go mod init ${packageid}
   deps=`cat ${installdir}/packages/${packageid}/${filename} | awk '/import/,/)/' | sed 's/import (//g' | head -n -1 | grep -v '//' | grep '\.' | tr -d '"' | sed 's/\t//g'`
   for dep in $deps
    do
    echo "downloading: $dep"
    go get $dep
    done
   if [ -f ${installdir}/packages/${packageid}/${packageid} ]; then rm ${installdir}/packages/${packageid}/${packageid}; fi
   echo "building binary..."
   go build
   if [ -f ${installdir}/packages/${packageid}/${packageid} ]; then chmod +x ${installdir}/packages/${packageid}/${packageid}; fi
   if [ -L ${installdir}/packages/bin/${packageid} ] || [ -e ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
   #ln -s ${installdir}/packages/${packageid}/${packageid} ${installdir}/packages/bin/${packageid}
   #link_binary "${installdir}/packages/${packageid}/${packageid}" "${packageid}"
   #link_binary "${installdir}/packages/${packageid}/${packageid}" "${installdir}/packages/bin/${packageid}"
   cd $curdir
   binpath="${installdir}/packages/${packageid}/${packageid}"
  fi

  if [ "$fileext" = 'deb' ]; then
   echo "Installing deb file..."
   if [ ! `check_installed dpkg` ]; then apt -y install dpkg 2>/dev/null || apk add -f dpkg 2>/dev/null; fi

   echo "Looking for depends"

  deps=`dpkg --info ${installdir}/packages/${packageid}/${filename} | \
    grep -E "Depends" | tr -d "|," | sed "s/([^)]*)/()/g" | tr -d "()" | sed "s/ /\n/g" | sort -r | uniq | grep -Ev "Depends"`

  for dep in $deps
   do
   echo "Depends: $dep"
  done

  depscount=$(echo $deps | wc -l)

   echo "Total Depend packages: $depscount"
   echo "Installing the main deb file"
   echo "Installing package for architecture: $(dpkg --print-architecture)"
   dpkg -i --dry-run --force-architecture ${installdir}/packages/${packageid}/${filename}
   if [ $? -ne 0 ] || [ "$depscount" > 0 ]; then
    echo "Extracting file via alternative method from repo"
    #apt -y install ${packageid} 2>/dev/null || apk add -f ${packageid} 2>/dev/null
    #apt -y --fix-broken install || apk-fix -f
    dpkg-deb -x ${installdir}/packages/${packageid}/${filename} ${installdir}/packages/${packageid}
    if [ ! -z $pbin ]; then mv ${pbin} ${pbin}.bak; fi

    binaryloc=$(check_installed "${packageid}")
    binpath="${binaryloc}"
    if [ ! -z "${binaryloc}" ]; then
     echo "Binary found: ${binaryloc}"
     chmod +x "${binaryloc}"

     if [ -L ${installdir}/packages/bin/${packageid} ] || [ -e ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
     #ln -s ${binaryloc} ${installdir}/packages/bin/${packageid}
     #link_binary "${binaryloc}" "${packageid}"
     #link_binary "${binaryloc}" "${installdir}/packages/bin/${packageid}"
    fi
   else
    if [ "$depscount" = 0 ]; then dpkg -i --force-architecture ${installdir}/packages/${packageid}/${filename}; fi
    #echo "error installing the deb"
   fi


  fi

  if [ "$fileext" = 'sh' ]; then
   echo "Installing shell file..."
   chmod +x ${installdir}/packages/${packageid}/${packageid}
   #if [ -f ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
   #ln -s ${installdir}/packages/${packageid}/${packageid} ${installdir}/packages/bin/${packageid}
   #link_binary "${installdir}/packages/${packageid}/${packageid}" "${packageid}"
   #link_binary "${installdir}/packages/${packageid}/${packageid}" "${installdir}/packages/bin/${packageid}"
   binpath="${installdir}/packages/${packageid}/${packageid}"
  fi

  if [ "$fileext" = 'dns' ]; then
   echo "Quering DNS script..."
   chmod +x ${installdir}/packages/${packageid}/${packageid}
   #if [ -f ${installdir}/packages/bin/${packageid} ]; then rm ${installdir}/packages/bin/${packageid}; fi
   #ln -s ${installdir}/packages/${packageid}/${packageid} ${installdir}/packages/bin/${packageid}
   #link_binary "${installdir}/packages/${packageid}/${packageid}" "${packageid}"
   #link_binary "${installdir}/packages/${packageid}/${packageid}" "${installdir}/packages/bin/${packageid}"
   binpath="${installdir}/packages/${packageid}/${packageid}"
  fi

 rm ${installdir}/packages/bin/${packageid} 2>/dev/null
 link_binary "${binpath}" "${installdir}/packages/bin/${packageid}"
 rm /etc/${packageid}/${packageid} 2>/dev/null
 link_binary "${binpath}" "/etc/${packageid}/${packageid}"
 echo "Installed: ${packageid}"
} #end of installpackage()

check_path() {

 if [ ! -d "${installdir}/packages/bin" ]; then mkdir -p ${installdir}/packages/bin; fi

 #setting up path
 echo "pminstalldir=$installdir" > ${installdir}/packages/bin/setpmpath
 cat >> "${installdir}/packages/bin/setpmpath" <<-' EOF'
 if ! echo "$PATH" | grep -q "${pminstalldir}/packages/bin"; then export PATH="${PATH}:${pminstalldir}/packages/bin"; fi
 EOF
 chmod +x ${installdir}/packages/bin/setpmpath

 if ! cat /etc/profile 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> /etc/profile; fi
 if ! cat /etc/profile.d/packagemanager.sh 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null > /etc/profile.d/packagemanager.sh; fi
 if ! cat /root/.profile 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> /root/.profile; fi
 if ! cat /root/.bashrc 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> /root/.bashrc; fi
 if ! cat /root/.zshrc 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> /root/.zshrc; fi

 homedir=''
 for homedir in $(find /home/ -maxdepth 1 2>/dev/null | grep -v '^/home/$' 2>/dev/null)
 do
 if ! cat ${homedir}/.profile 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> ${homedir}/.profile; fi
 if ! cat ${homedir}/.bashrc 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> ${homedir}/.bashrc; fi
 if ! cat ${homedir}/.zshrc 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> ${homedir}/.zshrc; fi
 done

 homedir=''
 for homedir in $(find /Users/ -maxdepth 1 2>/dev/null | grep -v '^/Users/$' 2>/dev/null)
 do
 if ! cat ${homedir}/.bash_profile 2>/dev/null | grep -q "$installdir/packages/bin"; then echo "source ${installdir}/packages/bin/setpmpath" 2>/dev/null >> ${homedir}/.profile; fi
 done

 if ! echo "$PATH" | grep -q "${installdir}/packages/bin"; then . ${installdir}/packages/bin/setpmpath 2>/dev/null; fi

 #echo "path is set to: $PATH"

}

check_commands() {
#commands needed curl/wget alt:gocurl, cat, grep, sed, cut, rev, sort, uniq, find, egrep, wc, head, awk, unzip, tar, gojq
if [ ! -z "${curlurl}" ]; then packurl=$(echo ${curlurl} | rev | cut -d '/' -f2- | rev | sed -e 's/$/\/packages.json/'); fi
if [ -z "${packurl}" ]; then packurl=$(cat ${installdir}/config.json | grep -i 'url' | cut -d ':' -f2- | sed 's/ \|\"\|,//g' | rev | cut -d '/' -f2- | rev | sed -e 's/$/\/packages.json/'); fi
if [ -z "${packurl}" ]; then echo "please pass url... exiting..."; exit; fi
if [ -f ${installdir}/packages.json ]; then packagesjson=`cat ${installdir}/packages.json`; fi

if [ ! -d ${installdir}/packages/bin ]; then mkdir -p ${installdir}/packages/bin; fi


#get arch and os
arch=`uname -m`
if [ "$(echo `uname -m`)" = 'x86_64' ]; then arch='amd64'; fi
binarchos=$(echo "gocurl-"`uname | tr [:upper:] [:lower:]`"-${arch}")

if [ ! `check_installed gocurl` ]; then
#echo "querying dns for file location of $binarchos"
if [ `check_installed dig` ]; then cid=`echo $(dig TXT +short _dnslink.binaries.$(hostname -f | cut -d. -f2- ). | tr -d '"' | cut -d'/' -f3- )`; fi
cid=$(echo $cid | awk '{gsub(/^[ \t]+|[ \t]+$/,"");print}')
if [ -z "$cid" ]; then cid=bafybeihhlmdvt4wnfhwnjhzdv5izndpomlxrt77gb4n5cnfccfa3mcb4z4; fi
binipfs=`echo "http://${cid}.ipfs.dweb.link/binaries.txt"`
#https://{cid}.ipfs.4everland.io

#via wget
 if ([ `check_installed wget` ] && [ ! `check_installed gocurl` ]); then
 echo "grab binaries file via wget from $binipfs"
 #wget -q $binipfs -O /tmp/binaries.temp
 retrydownload wget $binipfs /tmp/binaries.temp
 if [ ! -s /tmp/binaries.temp ]; then retrydownload wget $binipfs /tmp/binaries.temp; fi
 gocurllinkid=`cat /tmp/binaries.temp | grep "/$binarchos" | cut -d'.' -f1`

 echo "downloading $binarchos from cid: $gocurllinkid"
 #wget -q "https://${gocurllinkid}.ipfs.dweb.link/${binarchos}" -O /tmp/gl.temp
 retrydownload wget "https://${gocurllinkid}.ipfs.dweb.link/${binarchos}" /tmp/gl.temp
 if [ ! -s /tmp/gl.temp ]; then retrydownload wget "https://${gocurllinkid}.ipfs.dweb.link/${binarchos}" /tmp/gl.temp; fi

 #cp and make gocurl exec
 mkdir -p ${installdir}/packages/gocurl
 cp /tmp/gl.temp ${installdir}/packages/gocurl/gocurl
 chmod +x ${installdir}/packages/gocurl/gocurl
 link_binary ${installdir}/packages/gocurl/gocurl ${installdir}/packages/bin/gocurl
 #making gocurl avail via /etc/gocurl/gocurl
 mkdir -p /etc/gocurl
 link_binary ${installdir}/packages/gocurl/gocurl /etc/gocurl/gocurl

 fi #end wget if

#via curl
 if ([ `check_installed curl` ] && [ ! `check_installed gocurl` ]); then
 echo "grab binaries file via curl from $binipfs"

 #curl --doh-url https://1.1.1.1/dns-query -s $binipfs -o /tmp/binaries.temp
 retrydownload curl $binipfs /tmp/binaries.temp
 if [ ! -s /tmp/binaries.temp ]; then retrydownload curl $binipfs /tmp/binaries.temp; fi
 gocurllinkid=`cat /tmp/binaries.temp | grep "/$binarchos" | cut -d'.' -f1`

 echo "downloading $binarchos via curl from cid: $gocurllinkid"
 
 retrydownload curl "https://${gocurllinkid}.ipfs.dweb.link/${binarchos}" /tmp/gl.temp
 if [ ! -s /tmp/gl.temp ]; then retrydownload curl "https://${gocurllinkid}.ipfs.dweb.link/${binarchos}" /tmp/gl.temp; fi
 #curl --doh-url https://1.1.1.1/dns-query -s "https://${gocurllinkid}.ipfs.dweb.link/${binarchos}" -o /tmp/gl.temp

 #cp and make gocurl exec
 mkdir -p ${installdir}/packages/gocurl
 cp /tmp/gl.temp ${installdir}/packages/gocurl/gocurl
 chmod +x ${installdir}/packages/gocurl/gocurl
 link_binary ${installdir}/packages/gocurl/gocurl ${installdir}/packages/bin/gocurl
 #making gocurl avail via /etc/gocurl/gocurl
 mkdir -p /etc/gocurl
 link_binary ${installdir}/packages/gocurl/gocurl /etc/gocurl/gocurl

 fi #end curl if

#via nc
 if ([ `check_installed nc` ] && [ ! `check_installed gocurl` ]); then

 echo "grab binaries file via nc"
 #printf "%b\n" "GET /binaries.txt HTTP/1.1\r\nHost: ${cid}.ipfs.dweb.link\r\n\r\n" | nc ${cid}.ipfs.dweb.link 80 > /tmp/binaries.temp
 if [ "$(uname)" = "Darwin" ]; then NL=$'\\\n'; setopt interactive_comments; else NL="\n"; fi
 echo "GET /binaries.txt HTTP/1.1NLHost: ${cid}.ipfs.dweb.link:80NLUser-Agent: curl/1.0NLAccept: */*NLConnection: closeNLNL" | sed -e "s|NL|${NL}|g" | nc ${cid}.ipfs.dweb.link 80
 gocurllinkid=`cat /tmp/binaries.temp | grep "/$binarchos" | cut -d'.' -f1`
 #gocurllinkid=`cat /tmp/binaries.temp | grep "/$binarchos" | grep -E -o '[a-z0-9]{46}\.' | tr -d'.' | tr -d '/'`
 #rm /tmp/binaries.temp

 echo "downloading $binarchos from cid: $gocurllinkid"
 printf "%b\n" "GET /$binarchos HTTP/1.1\r\nHost: ${gocurllinkid}.ipfs.dweb.link\r\n\r\n" | nc ${gocurllinkid}.ipfs.dweb.link 80 > /tmp/gl.temp

 #convert to file
 mkdir -p ${installdir}/packages/gocurl
 tail -n +$(echo $(grep --text -n '^.$' /tmp/gl.temp | head -n 1 | tr -d ':'| sed 's/\r\|\n//g')) /tmp/gl.temp > /tmp/gocurl.tmp
 tail -n +2 /tmp/gocurl.tmp > ${installdir}/packages/gocurl/gocurl
 #make gocurl exec
 chmod +x ${installdir}/packages/gocurl/gocurl
 link_binary ${installdir}/packages/gocurl/gocurl ${installdir}/packages/bin/gocurl
 #making gocurl avail via /etc/gocurl/gocurl
 mkdir -p /etc/gocurl
 link_binary ${installdir}/packages/gocurl/gocurl ${installdir}/gocurl/gocurl
fi # end nc

fi # end gocurl if

#get packages.json now gocurl is installed
if [ ! -f ${installdir}/packages.json ]; then packagesjsonloc="/tmp/packages.json"; fi
if [ -d ${installdir} ]; then packagesjsonloc="${installdir}/packages.json"; fi # if already installed
if [ -f ${installdir}/packages.json ]; then packagesjsonloc="${installdir}/packages.json"; fi
packagesjson=`cache_file $packurl ${packagesjsonloc}`

#install gojq


if [ ! `check_installed gojq` ]; then echo "Installing required command: gojq"; mkdir -p ${installdir}/packages/gojq; packageurl=$(echo "$packagesjson" | grep 'gojq' | grep `echo \"$(uname) | tr '[:upper:]' '[:lower:]'` | cut -d ':' -f2- | tr -d '"\|,\| '); packagefilename=`echo $packageurl | rev | cut -d '/' -f1 | rev`; cache_file $packageurl ${installdir}/packages/gojq/${packagefilename}; installpackage $packagefilename gojq; fi

badlink=$(find ${installdir}/packages/bin/gojq -type l ! -exec test -e {} \; -print)
if [ ! -z "$badlink" ]; then
    echo "gojq is BROKEN...Resetting... Please restart"
    rm ${installdir}/packages/bin/gojq
    rm -rf ${installdir}/packages/gojq
    mkdir -p ${installdir}/packages/gojq
    exit
fi

} # end of check_commands

#-------------------------------------------------End of Functions------------------------------------------------------


if [ ! -z "${curlurl}" ]; then
 echo "Downloading script from $curlurl...";
 if [ ! -d ${installdir} ]; then mkdir -p ${installdir}; fi
 if [ ! -f ${installdir}/config.json ]; then echo "{ }" > ${installdir}/config.json; fi
 check_path
 check_commands
 /etc/gojq/gojq ".\"url\" = \"${curlurl}\" " ${installdir}/config.json > json.tmp && mv json.tmp ${installdir}/config.json
fi


if [ "$listpackages" = 'yes' ]; then
  if [ -f ${installdir}/config.json ]; then
  curlurl=`${installdir}/packages/bin/gojq ".\"url\"" ${installdir}/config.json | tr -d '"'`
   if [ ! -z "$curlurl" ]; then
   packurl=`echo $curlurl | rev | cut -d'/' -f2- | rev | sed -e 's/$/\/packages.json/'`
   packagesjson=`cache_file $packurl ${installdir}/packages.json`
   fi
  fi
  if [ -f ${installdir}/packages.json ]; then
    check_commands
    echo "Available Packages:"
    packages=`cat ${installdir}/packages.json | ${installdir}/packages/bin/gojq .packages | ${installdir}/packages/bin/gojq keys | sed 's/\[\|\]\|,\|\"\|^$\|[[:blank:]]//g' | tr -d '"\|[\|]\|,' | sed '/^$/d' | sort -u`
    for package in $(echo $packages)
    do
     printf $package
     if [ -d ${installdir}/packages/${package} ]; then printf "%b\n" " (installed)"; fi
     if [ ! -d ${installdir}/packages/${package} ]; then printf "%b\n" " "; fi
    done
  else
    echo "No Packages available or packages.json not found"
  fi
exit
fi

if [ "$shouldinstall" = 'yes' ]; then
 echo "Installing to ${installdir}...";
 check_path
 check_commands

 if [ ! -d ${installdir} ]; then mkdir -p ${installdir}; fi
 if [ ! -f ${installdir}/config.json ]; then echo "{ }" > ${installdir}/config.json; fi
 curlurl=`/etc/gojq/gojq ".\"url\"" ${installdir}/config.json | tr -d '"' | sed 's/^null$//g'`
 /etc/gojq/gojq ".\"version\" = \"${pmversion}\" " ${installdir}/config.json > json.tmp && mv json.tmp ${installdir}/config.json
 if [ -z "${curlurl}" ]; then echo "Please rerun the script with the --url parameter"; exit; fi
 if [ ! -z "${curlurl}" ]; then
  gocurl ${curlurl} > ${installdir}/pm.sh.tmp
  if [ -s ${installdir}/pm.sh.tmp ]; then
   mv ${installdir}/pm.sh.tmp ${installdir}/pm.sh
   chmod +x ${installdir}/pm.sh
   ${installdir}/packages/bin/setpmpath
  else
   echo "Error: please connect to internet to download new copy and rerun command"
  fi
 fi
exit
fi

if [ "$shouldremove" = 'yes' ]; then
 export PATH=$(echo $PATH | sed 's,'"${installdir}/packages/bin"',,' | sed 's/::/:/g' | sed 's/:$//g')
 if [ ! -z "${installdir}" ]; then echo "Removing ${installdir}..."; rm -rf ${installdir}; echo "Done. Program removed."; fi
 if [ -z "${installdir}" ]; then echo "Program not found. Unable to remove"; fi
fi


if [ ! -z $packageid ]; then
 check_commands
 if [ $packageid = 'all' ]; then

  echo "Scanning Directories for packages to update..."
  packages=$(cat ${installdir}/packages.json | ${installdir}/packages/bin/gojq .packages | ${installdir}/packages/bin/gojq keys | awk 'gsub(/ |\"|\]|\[|\,/,""); { print }' 2>/dev/null | awk '!seen[$0]++')
  for package in $(echo $packages)
  do
  if [ -e "/etc/${package}" ]; then 
    #echo "Found $package installed..."
    if [ ! -e "${installdir}/packages/${package}" ]; then
    echo "Found $package installed... adding to packagemanager update system"
    mkdir -p ${installdir}/packages/${package}
    fi
  fi
  done
  echo "Done Scanning Directories"
  echo ""


 packagesinstalled=`find $installdir/packages -type d 2>/dev/null | sed 's,'"${installdir}/packages/"',,' | cut -d '/' -f1 | sort -u | grep -v '^bin$'`
 echo "Preparing to upgrade all $(echo ${packagesinstalled} | wc -w) packages"
 echo "0" > $installdir/.countupgraded
 for package in $packagesinstalled
 do
  echo "upgrading package: $package"
  echo "running: $0 upgrade $package"
  $0 upgrade $package
 done
 echo "All $(echo ${packagesinstalled} | wc -w) packages should be up to date"
 if [ $(cat $installdir/.countupgraded) -gt 0 ]; then echo "upgraded $(cat $installdir/.countupgraded) package(s)"; rm $installdir/.countupgraded; fi
 exit
 fi


 echo "Install/Update package: $packageid";

 if [ $(echo $packageid | awk '$1 ~ /\.dns$/ {print $0; rc = 1}; END { exit !rc }') ]; then
 echo "Looks like a DNS package... trying to install/update"
 os=$(uname | tr '[:upper:]' '[:lower:]')
 #echo "OS: $os"
 arch=$(uname -m | tr '[:upper:]' '[:lower:]')
 #echo "Arch: $arch"
 dnsrec=$(echo ${packageid}.$(hostname -f | cut -d. -f2- ). | tr -d '"' | awk '// {sub(/\.dns\./, ".'"${os}"'_'"${arch}"'.package."); print}')
 #echo "dnsrec: $dnsrec"
 dohrec=`echo "https://8.8.8.8/resolve?name=${dnsrec}&type=txt"`
 #echo "dohrec: $dohrec"
 dnsver="${installdir}/packages/${packageid}/.dns-${packageid}"
 if [ ! -e "${installdir}/packages/${packageid}" ]; then mkdir -p ${installdir}/packages/${packageid}; fi
 touch $dnsver
 filerec=`awk '{ print }' ${dnsver} | awk '/"Answer"/,EOF' | awk '// {gsub(/,"/, "\n"); print}' | awk '{gsub(/"|}|\]|\[|\{/,"");print}' | awk '/data:/{gsub("data:","",$1); print $0}' | awk '{gsub(/^[ \t]+|[ \t]+$/,"");print}'`
 #echo "filerec: $filerec"
 txtrec=`cache_file $dohrec ${dnsver} | awk '/"Answer"/,EOF' | awk '// {gsub(/,"/, "\n"); print}' | awk '{gsub(/"|}|\]|\[|\{/,"");print}' | awk '/data:/{gsub("data:","",$1); print $0}' | awk '{gsub(/^[ \t]+|[ \t]+$/,"");print}'`
 #echo "txtrec: $txtrec"
 #echo "dnsver: $dnsver"
 
 if [ "$txtrec" != "$filerec" ]; then 
  echo "DNS TXT content doesnt match... (${txtrec} != ${filerec}) updating"
  packageiddns=$( echo $packageid | awk '// {sub(/\.dns/, ""); print}');
  if [ ! -z "$txtrec" ]; then echo "redownloading file... ${txtrec}"; scripttxt=$(cache_file $txtrec ${installdir}/packages/${packageid}/${packageiddns}); chmod +x ${installdir}/packages/${packageid}/${packageiddns}; link_binary "${installdir}/packages/${packageid}/${packageiddns}" "${installdir}/packages/bin/${packageiddns}"; fi
 fi
 if [ -z "$txtrec" ]; then echo "No DNS record found for $packageid at $dnsrec"; echo ""; exit; fi
 if [ "$(version $txtrec)" = "$(version $filerec)" ]; then echo "${packageid} Local version is up to date with the Remote Version. Quiting..."; fi
 echo ""; exit
 fi

 pbin=''
 pbin=`check_installed ${packageid}`
 pbin=$(echo $pbin | awk '! /\/etc\/'"$packageid"'/')

 if [ -z "$pbin" ]; then
  echo "$packageid is not installed locally... changing mode to install"
 fi

 if [ ! -z "$pbin" ]; then
  echo "$packageid is installed... getting version... changing mode to upgrade"
  lversion=$(${pbin} --version 2>&1 | egrep -o "([0-9]{1,}\.)+[0-9]{1,}" | tr -d '"' | head -n 1)
  if [ -z $lversion ]; then lversion=$(${pbin} -v 2>&1 | egrep -o "([0-9]{1,}\.)+[0-9]{1,}" | tr -d '"' | head -n 1); fi
  if [ -z $lversion ]; then lversion=$(${pbin} -V 2>&1 | egrep -o "([0-9]{1,}\.)+[0-9]{1,}" | tr -d '"' | head -n 1); fi
  if [ -z $lversion ]; then lversion=$(${pbin} version 2>&1 | egrep -o "([0-9]{1,}\.)+[0-9]{1,}" | tr -d '"' | head -n 1); fi
  if [ -z $lversion ]; then lversion=$(${pbin} 2>&1 | egrep -o "([0-9]{1,}\.)+[0-9]{1,}" | tr -d '"' | head -n 1); fi
  if [ -z $lversion ]; then lversion='1'; fi
  echo "Local version: ${lversion}"
  echo "${packageid} installed: ${pbin}"
 fi

 curlurl=`${installdir}/packages/bin/gojq ".\"url\"" ${installdir}/config.json | tr -d '"'`
 packurl=`echo $curlurl | rev | cut -d'/' -f2- | rev | sed -e 's/$/\/packages.json/'`
 #echo "Updating packages.json from $packurl to ${installdir}/packages.json"
 packagesjson=`cache_file $packurl ${installdir}/packages.json`
 if [ ! -f ${installdir}/packages.json ]; then echo "package.json doesnt exist. Please pass --url param or upd url in config"; exit; fi
 rversion=`echo $packagesjson | ${installdir}/packages/bin/gojq ".\"packages\".\"${packageid}\".\"version\"" | tr -d '"'`

 if [ "${rversion}" = 'null' ]; then echo "No Remote Package version found for ${packageid}. Quitting..."; echo ""; exit; fi

 if [ "$(version ${rversion})" = "$(version ${lversion})" ]; then echo "${packageid} Local Version (${lversion}) is up to date with the Remote Version (${rversion}). Quitting..."; echo ""; if [ ! -d "${installdir}/packages/${packageid}" ]; then mkdir -p ${installdir}/packages/${packageid}; fi; exit; fi

if [ ! -z "$lversion" ]; then
 if [ "$(version ${lversion})" -gt "$(version ${rversion})" ]; then echo "${packageid} Local Version (${lversion}) is newer than the Remote Version (${rversion}). Quitting..."; echo ""; exit; fi
 fi

 if [ "${rversion}" != 'null' ]; then
  echo "Remote version: ${rversion}";
  os=$(uname | tr '[:upper:]' '[:lower:]')
  echo "OS: $os"
  arch=$(uname -m | tr '[:upper:]' '[:lower:]')
  echo "Arch: $arch"
  rurl=`echo $packagesjson | ${installdir}/packages/bin/gojq ".\"packages\".\"${packageid}\".\"archuri\".\"${os}_${arch}\"" | tr -d '"'`
  filename=`echo ${rurl} | rev | cut -d'/' -f1 | rev`
  if [ -z "$filename" ] || [ "$filename" = 'null' ]; then echo "Not a valid url. Quitting..."; echo ""; exit; fi
  echo "Downloading... $rurl"
  if [ ! -d ${installdir}/packages/${packageid} ]; then mkdir -p ${installdir}/packages/${packageid}; fi
  if ! echo ${filename} | grep -qi '\.'; then filename=${packageid}; fi
  if [ ! -z "$rurl" ]; then txt=`cache_file ${rurl} ${installdir}/packages/${packageid}/${filename}`; fi
  if [ ! -s ${installdir}/packages/${packageid}/${filename} ]; then echo "File not downloaded. Quitting..."; echo ""; exit; fi
  if [ -s ${installdir}/packages/${packageid}/${filename} ]; then
   echo "File: $filename successfully downloaded"
   #if [ ! -z "${pbin}" ]; then mv ${pbin} ${pbin}.bak; fi
   echo "Removing/Cleaning previous package directory: ${installdir}/packages/${packageid}"
   find ${installdir}/packages/${packageid}/* ! -name "$filename" -mtime +1 -exec rm -rv {} +
   packagesupgraded=$(cat $installdir/.countupgraded)
   installpackage ${filename} ${packageid}
   packagesupgraded=$((packagesupgraded+1))
   echo $packagesupgraded > $installdir/.countupgraded
  fi

 fi
fi


printf "%b\n" "Done\n"
