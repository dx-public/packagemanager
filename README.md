# packagemanager



## Getting started

Linux:
```
curlurl='https://gitlab.com/dx-public/packagemanager/-/raw/main/pm.sh' && (wget $curlurl -O - -q 2>/dev/null || curl -s $curlurl 2>/dev/null) | sh -s install --url $curlurl
```

Windows (in Powershell):
```
powershell.exe -exec bypass -c {Set-Variable curlurl https://gitlab.com/dx-public/packagemanager/-/raw/main/pm.ps1;iex "& { $(irm (Get-Variable curlurl -Value)) } --url (Get-Variable curlurl -Value) install"}
```

MacOS:
```
curlurl='https://gitlab.com/dx-public/packagemanager/-/raw/main/pm.sh' && curl -s $curlurl | bash -s install --url $curlurl
```


***

## Name
Package Manager

## Description
Package Manager for Unix/Linux, Windows, MacOS


## tools
https://json-gui.esstudio.site/

