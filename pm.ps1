# This is the file for Windows (Powershell)

$version = "0.1"
$installdir = "$env:ProgramData\packagemanager"

#write-host "There are a total of $($args.count) arguments"
if($args.count -eq 0){
write-host "Please pass an option:"
write-host "install - update/install script"
write-host "self-update - update script"
write-host "update <pkg> - update/install package"
write-host "upgrade <pkg> - upgrade/install package"
write-host "remove - remove package manager"
write-host "uninstall - remove package manager"

exit
}


for ( $i = 0; $i -lt $args.count; $i++ ) {
    #write-host "Argument  $i is $($args[$i])"
    if($($args[$i]) -eq "-v") {write-host "version is $version"; exit}
    if($($args[$i]) -eq "--version") {write-host "version is $version"; exit}
    if($($args[$i]) -eq "version") {write-host "version is $version"; exit}
    if($($args[$i]) -eq "-version") {write-host "version is $version"; exit}
    if($($args[$i]) -eq "-u") {$url=$($args[$i+1]);}
    if($($args[$i]) -eq "--url") {$url=$($args[$i+1]);}
    if($($args[$i]) -eq "install") {$shouldinstall='yes';}
    if($($args[$i]) -eq "self-update") {$shouldinstall='yes';}
} 

Write-Host ""
Write-Host "Package Manager for Windows"
Write-Host ""


if (![string]::IsNullOrEmpty($shouldinstall))
{
    echo "Installing to $installdir...";

    if(!(Test-Path $installdir))
    {
    Write-Host "Creating Directory..."
    New-Item -Path "$installdir" -ItemType Directory
    }

    if(!(Test-Path $installdir\config.json))
    {
    $configjson = @{}
    $configjson.Add("version",$version)
    if ([string]::IsNullOrEmpty($url)){write-host "Please pass a url in"; exit;}
    $configjson.Add("url",$url)
    $configjson | ConvertTo-Json | Out-File "$installdir\config.json"
    }

    if (Get-Variable url -ErrorAction Ignore)
    {
    #Write-Host "Downloading from $url"
    $configjson=Get-Content -Raw "$installdir\config.json" | ConvertFrom-Json
    Write-Host "url passed...Changing config url from $(${configjson}.url) to $url"
    $configjson.url="$url"
    $configjson | ConvertTo-Json | Out-File "$installdir\config.json"
    Remove-Variable url
    $url=''
    $configjson=Get-Content -Raw "$installdir\config.json" | ConvertFrom-Json
    }

    $configjson=Get-Content -Raw "$installdir\config.json" | ConvertFrom-Json
    Write-Host "Downloading Powershell Program Manager from url"
    Invoke-WebRequest $configjson.url -OutFile $installdir\pm.ps1

    Write-Host "Setting Permissions"
    try{
    Set-ExecutionPolicy RemoteSigned
    }
    catch {
        #Do Nothing
    }
    Unblock-File -Path $installdir\pm.ps1

}



Write-Host ""
Write-Host "Done"
Write-Host ""
